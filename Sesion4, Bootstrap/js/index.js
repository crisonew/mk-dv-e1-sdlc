var ProductosObtenidos;
var clientesObtenidos;
var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-"

function getProductos()
{
	var url="https://services.odata.org/V4/Northwind/Northwind.svc/Products";
	var request=new XMLHttpRequest();

	request.onreadystatechange = function(){
		if(this.readyState==4 && this.status==200){
			console.table(JSON.parse(request.responseText).value);
			ProductosObtenidos = request.responseText;
			procesasProductos();
		}
	}

	request.open("GET", url, true);
	request.send();
}

function getClientes()
{
	//var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'";
	var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
	var request=new XMLHttpRequest();

	request.onreadystatechange = function(){
		if(this.readyState==4 && this.status==200){
			//console.table(JSON.parse(request.responseText).value);
			clientesObtenidos = request.responseText;
			procesasClientes();
		}
	}

	request.open("GET", url, true);
	request.send();
}

function procesasProductos(argument) {
	var JSONClientes = JSON.parse(ProductosObtenidos);
	var divTabla= document.getElementById("divTabla");
	var tabla= document.createElement("table");
	var tbody= document.createElement("tbody");

	tabla.classList.add("table");
	tabla.classList.add("table-striped");

	for (var i = 0; i< JSONClientes.value.length; i++) {
		//console.log(JSONClientes.value[i].ProductName);
		var nuevaFila=document.createElement("tr");
		var columnaNombre = document.createElement("td");
		columnaNombre.innerText = JSONClientes.value[i].ProductName;

		var columnaPrice = document.createElement("td");
		columnaPrice.innerText = JSONClientes.value[i].UnitPrice;

		var columnaStock = document.createElement("td");
		columnaStock.innerText = JSONClientes.value[i].UnitsInStock;

		nuevaFila.appendChild(columnaNombre);
		nuevaFila.appendChild(columnaPrice);
		nuevaFila.appendChild(columnaStock);

		tbody.appendChild(nuevaFila);

	}
	tabla.appendChild(tbody);
	divTabla.appendChild(tabla);
	//alert(JSONClientes.value[5].ProductName);
}

function procesasClientes(argument) {
	var JSONClientes = JSON.parse(ProductosObtenidos);
	var divTabla= document.getElementById("divTabla");
	var tabla= document.createElement("table");
	var tbody= document.createElement("tbody");

	tabla.classList.add("table");
	tabla.classList.add("table-striped");

	for (var i = 0; i< JSONClientes.value.length; i++) {
		//console.log(JSONClientes.value[i].ProductName);
		var nuevaFila=document.createElement("tr");
		var columnaNombre = document.createElement("td");
		columnaNombre.innerText = JSONClientes.value[i].ProductName;

		var columnaPrice = document.createElement("td");
		columnaPrice.innerText = JSONClientes.value[i].UnitPrice;

		var columnaStock = document.createElement("td");
		columnaStock.innerText = JSONClientes.value[i].UnitsInStock;

		nuevaFila.appendChild(columnaNombre);
		nuevaFila.appendChild(columnaPrice);
		nuevaFila.appendChild(columnaStock);

		tbody.appendChild(nuevaFila);

	}
	tabla.appendChild(tbody);
	divTabla.appendChild(tabla);
	//alert(JSONClientes.value[5].ProductName);
}

function procesasClientes(argument) {
	var JSONClientes = JSON.parse(clientesObtenidos);

	var divTabla= document.getElementById("divTablaCustomer");
	var tabla= document.createElement("table");
	var tbody= document.createElement("tbody");

	tabla.classList.add("table");
	tabla.classList.add("table-striped");

	for (var i = 0; i< JSONClientes.value.length; i++) {

		var nuevaFila=document.createElement("tr");
		var columnaNameCustomer = document.createElement("td");
		columnaNameCustomer.innerText = JSONClientes.value[i].ContactName;

		var columnaCity = document.createElement("td");
		columnaCity.innerText = JSONClientes.value[i].City;

		var cityFlag= document.createElement("td");
		var imgBandera = document.createElement("img");
		if (JSONClientes.value[i].Country=="UK") 
		{	imgBandera.src = "https://www.countries-ofthe-world.com/flags-normal/flag-of-United-Kingdom.png"
		}else{
		imgBandera.src = rutaBandera+ JSONClientes.value[i].Country+ ".png";
		}

		cityFlag.appendChild(imgBandera);

		nuevaFila.appendChild(columnaNameCustomer);
		nuevaFila.appendChild(columnaCity);
		nuevaFila.appendChild(cityFlag);

		tbody.appendChild(nuevaFila);

	}
	tabla.appendChild(tbody);
	divTabla.appendChild(tabla);
	//alert(JSONClientes.value[5].ProductName);
}